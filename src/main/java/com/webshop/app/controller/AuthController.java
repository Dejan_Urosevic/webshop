package com.webshop.app.controller;

import java.util.Collections;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.webshop.app.dto.LoginRequest;
import com.webshop.app.dto.SignUpRequest;
import com.webshop.app.enumeration.RoleName;
import com.webshop.app.exceptions.AppException;
import com.webshop.app.model.Role;
import com.webshop.app.model.User;
import com.webshop.app.payload.ApiResponse;
import com.webshop.app.payload.JwtAuthencitationResponse;
import com.webshop.app.repository.RoleRepository;
import com.webshop.app.repository.UserRepository;
import com.webshop.app.security.JwtTokenProvider;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

	private AuthenticationManager authenticationManager;
	private UserRepository userRepository;
	private RoleRepository roleRepository;
	private PasswordEncoder passwordEncoder;
	private JwtTokenProvider jwtTokenProvider;
	
	public AuthController(AuthenticationManager authenticationManager, UserRepository userRepository,
			RoleRepository roleRepository, PasswordEncoder passwordEncoder, JwtTokenProvider jwtTokenProvider) {
		this.authenticationManager = authenticationManager;
		this.userRepository = userRepository;
		this.roleRepository = roleRepository;
		this.passwordEncoder = passwordEncoder;
		this.jwtTokenProvider = jwtTokenProvider;
	}
	
	
	@PostMapping("/login")
	public ResponseEntity<?> login(@Valid @RequestBody LoginRequest loginRequest) {
		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsernameOrEmail(),
						loginRequest.getPassword())
				);
		
		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtTokenProvider.generateToken(authentication);
		return ResponseEntity.ok(new JwtAuthencitationResponse(jwt));
	}
	
	@PostMapping("/register")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
		if(userRepository.existsByUsername(signUpRequest.getUsername())) {
			return new ResponseEntity(new ApiResponse(false, "Username is already taken!"), HttpStatus.BAD_REQUEST);
		}
		
		if(userRepository.existsByEmail(signUpRequest.getEmail())) {
			return new ResponseEntity(new ApiResponse(false, "Email is already in use!"), HttpStatus.BAD_REQUEST);
		}
		
		User user = new User(signUpRequest.getFirstname(), signUpRequest.getSecondname(), 
				signUpRequest.getUsername(), signUpRequest.getPhone(), signUpRequest.getEmail());
		
		user.setPassword(passwordEncoder.encode(signUpRequest.getPassword()));
		
		Role role = roleRepository.findByName(RoleName.ROLE_USER).orElseThrow(() -> new AppException("User Role not set."));
		
		user.setRoles(Collections.singleton(role));

		user.setRoles(Collections.singleton(role));
		User savedUser = userRepository.save(user);
		
		return ResponseEntity.created(
				ServletUriComponentsBuilder
					.fromCurrentContextPath().path("/api/users/{username}")
					.buildAndExpand(savedUser.getUsername()).toUri()).body(new ApiResponse(true, "User registrated successfully"));
 	}
	
}
