package com.webshop.app.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.webshop.app.repository.ProductRepository;

@RestController
@RequestMapping("/api/product")
public class ProductController {

	private ProductRepository productRepository;
	
	public ProductController(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}
	
	@GetMapping
	public ResponseEntity<?> getAllProduct() {
		return ResponseEntity.ok(productRepository.findAll());
	}
}
