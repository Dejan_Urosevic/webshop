package com.webshop.app.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
public class ActionDto {

	@Getter
	@Setter
	private Date startDate;
	
	@Getter
	@Setter
	private Date expreDate;
	
	@Getter
	@Setter
	private Double percent;
}
