package com.webshop.app.dto;

import com.webshop.app.model.Action;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
public class CategoryDto {

	@Getter
	@Setter
	private String title;
	
	@Getter
	@Setter
	private Action action;
	
	@Getter
	@Setter
	private CategoryDto categoryDto;
}
