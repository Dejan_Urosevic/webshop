package com.webshop.app.dto;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
public class LoginRequest {

	@NotBlank
	@Getter
	@Setter
	private String usernameOrEmail;
	
	@NotBlank
	@Getter
	@Setter
	private String password;
}
