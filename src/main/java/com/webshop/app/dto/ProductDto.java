package com.webshop.app.dto;

import java.sql.Blob;

import com.webshop.app.enumeration.Currency;
import com.webshop.app.model.Action;
import com.webshop.app.model.Category;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
public class ProductDto {

	@Getter
	@Setter
	private String title;
	
	@Getter
	@Setter
	private String description;
	
	@Getter
	@Setter
	private Double unitPrice;
	
	@Getter
	@Setter
	private Currency currency;
	
	@Getter
	@Setter
	private Category category;
	
	@Getter
	@Setter
	private Blob image;
	
	@Getter
	@Setter
	private Action action;
}
