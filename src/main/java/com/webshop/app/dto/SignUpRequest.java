package com.webshop.app.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
public class SignUpRequest {

	@NotBlank
	@Getter
	@Setter
	private String firstname;
	
	@NotBlank
	@Getter
	@Setter
	private String secondname;
	
	@NotBlank
	@Getter
	@Setter
	private String username;
	
	@NotBlank
	@Getter
	@Setter
	@Size(min = 6, max = 40)
	private String password;
	
	@NotBlank
	@Getter
	@Setter
	private String email;
	
	@Getter
	@Setter
	private String phone;
}
