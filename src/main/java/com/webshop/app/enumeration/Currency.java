package com.webshop.app.enumeration;

public enum Currency {

	RSD("rsd", "Serbian Dinar"),
	EUR("eur", "Euro"),
	USD("usd", "Dollar");
	
	private String shortcut;
	private String name;
	
	Currency(String shortcut, String name) {
		this.shortcut = shortcut;
		this.name = name;
	}
	
	public String getShortcut() {
		return shortcut;
	}
	
	public String getName() {
		return name;
	}
}
