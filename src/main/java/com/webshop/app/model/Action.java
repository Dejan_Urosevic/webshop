package com.webshop.app.model;

import javax.persistence.Table;

import com.webshop.app.model.audit.DataAudit;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@SuppressWarnings("serial")
@Entity
@Table(name = "action")
@NoArgsConstructor
@AllArgsConstructor
public class Action extends DataAudit {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "startdate", nullable = false)
	@Getter
	@Setter
	private Date startDate;
	
	@Column(name = "expiredate")
	@Getter
	@Setter
	private Date exipireDate;
	
	@Column(name = "percent", nullable = false)
	@Getter
	@Setter
	private Double percent;
}
