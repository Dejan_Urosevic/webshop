package com.webshop.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "category")
@NoArgsConstructor
@AllArgsConstructor
public class Category {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private @Getter Long id;
	
	@Column(name = "title", nullable = false)
	@Getter
	@Setter
	private String title;
	
	@ManyToOne
	@JoinColumn(name = "action_id")
	@Getter
	@Setter
	private Action action;
	
	@ManyToOne
	@JoinColumn(name = "parentcategory_id")
	@Getter
	@Setter
	private Category parentCategory;
}
