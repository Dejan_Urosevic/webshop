package com.webshop.app.model;

import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.webshop.app.enumeration.Currency;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "product")
@NoArgsConstructor
@AllArgsConstructor
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private @Getter Long id;
	
	@Column(name = "title", nullable = false)
	@Getter
	@Setter
	private String title;
	
	@Column(name = "description")
	@Getter
	@Setter
	private String description;
	
	@Column(name = "unitprice", nullable = false)
	@Getter
	@Setter
	private Double unitPrice;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "currency")
	@Getter
	@Setter
	private Currency currency;

	@ManyToOne
	@JoinColumn(name = "category_id", nullable = false)
	@Getter
	@Setter
	private Category category;
	
	@Column(name = "image")
	@Getter
	@Setter
	private Blob image;
	
	@ManyToOne
	@JoinColumn(name = "action_id")
	@Getter
	@Setter
	private Action action;
	
}
