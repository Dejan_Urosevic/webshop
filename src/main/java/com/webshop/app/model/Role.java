package com.webshop.app.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.webshop.app.enumeration.RoleName;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "role")
public class Role {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private @Getter Long id;
	
	@Enumerated(EnumType.STRING)
	@Getter
	@Setter
	private RoleName name;
	
	
}
