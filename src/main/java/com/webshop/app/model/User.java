package com.webshop.app.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.NaturalId;

import com.webshop.app.model.audit.DataAudit;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@SuppressWarnings("serial")
@Entity
@Table(name = "user")
@NoArgsConstructor
public class User extends DataAudit {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private @Getter Long id;
	
	@Column(name = "firstname", nullable = false)
	@Getter
	@Setter
	private String firstname;
	
	@Column(name = "secondname", nullable = false)
	@Getter
	@Setter
	private String secondname;
	
	@NaturalId
	@Column(name = "username", nullable = false, unique = true)
	@Getter
	@Setter
	private String username;
	
	@Column(name = "password", nullable = false)
	@Getter
	@Setter
	private String password;
	
	@Column(name = "phone")
	@Getter
	@Setter
	private String phone;
	
	@Column(name = "email", nullable = false, unique = true)
	@Getter
	@Setter
	private String email;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "user_roles",
		joinColumns = @JoinColumn(name = "user_id"),
		inverseJoinColumns = @JoinColumn(name = "role_id"))
	@Getter
	@Setter
	private Set<Role> roles = new HashSet<>();

	public User(String firstname, String secondname, String username, String phone, String email) {
		super();
		this.firstname = firstname;
		this.secondname = secondname;
		this.username = username;
		this.phone = phone;
		this.email = email;
	}
}
