package com.webshop.app.payload;

import lombok.Getter;
import lombok.Setter;

public class JwtAuthencitationResponse {

	@Getter
	@Setter
	private String accessToken;
	
	@Getter
	@Setter
	private String tokenType = "Bearer";

	public JwtAuthencitationResponse(String accessToken) {
		this.accessToken = accessToken;
	}
	
}
