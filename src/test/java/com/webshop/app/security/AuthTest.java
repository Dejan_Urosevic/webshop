package com.webshop.app.security;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.webshop.app.dto.LoginRequest;
import com.webshop.app.dto.SignUpRequest;
import com.webshop.app.model.Role;
import com.webshop.app.enumeration.RoleName;
import com.webshop.app.repository.RoleRepository;
import com.webshop.app.service.CustomUserDetailsService;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AuthTest {
	
	@Autowired
	private MockMvc mvc;
	
	@Autowired
	private RoleRepository roleRepository;
		
	@MockBean
	private AuthenticationManager authenticationManager;
	
	@MockBean
	private CustomUserDetailsService customUserDetailsService;
	
	@MockBean
	private JwtTokenProvider jwtTokenProvider;
	
	@Test
	public void test1_insertRole() throws Exception {
		Role user_role = new Role();
		user_role.setName(RoleName.ROLE_USER);
		
		Role savedRole = roleRepository.save(user_role);
		assertEquals(RoleName.ROLE_USER, savedRole.getName());
		
		Role admin_role = new Role();
		admin_role.setName(RoleName.ROLE_ADMIN);
		
		Role savedRole1 = roleRepository.save(admin_role);
		assertEquals(RoleName.ROLE_ADMIN, savedRole1.getName());
	}
	
	@Test
	public void test2_registerUser() throws Exception {
		SignUpRequest user = new SignUpRequest("test", "test", "username1", "password", "test1@test.com", "123");
				
		mvc.perform(post("/api/auth/register")
				.contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(user)))
			.andExpect(status().isCreated());
	}
	
	@Test
	public void test3_successfulLogin() throws Exception {
		
		LoginRequest loginRequest = new LoginRequest("username1", "password");
		
		mvc.perform(post("/api/auth/login")
				.contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(loginRequest)))
			.andExpect(status().isOk());
	}
	
	@Test
	public void test4_shouldNotAllowAccess() throws Exception {
		mvc.perform(get("/api/user")).andExpect(status().isUnauthorized());
	}
	
}
